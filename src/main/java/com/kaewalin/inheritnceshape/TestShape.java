/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.inheritnceshape;

/**
 *
 * @author ACER
 */
public class TestShape {

    public static void main(String[] args) {
        Shape shape = new Shape("Shape");
        shape.calArea();
        shape.ShowArea();
        
        Circle circle1 = new Circle("circle1", 3);
        circle1.calArea();
        circle1.ShowArea();

        Circle circle2 = new Circle("circle2", 4);
        circle2.calArea();
        circle2.ShowArea();
        
        Triangle triangle = new Triangle("triangle", 3, 4);
        triangle.calArea();
        triangle.ShowArea();
        
        Rectangle rectangle = new Rectangle("rectangle", 3, 4);
        rectangle.calArea();
        rectangle.ShowArea();
        
        Square square = new Square("square", 2);
        square.calArea();
        square.ShowArea();

        System.out.println("---------------------");
        
        Shape[] shapes = {circle1, circle2, triangle, rectangle, square};

        for (int i = 0; i < shapes.length; i++) {
            shapes[i].ShowArea();
        }

    }
}


       
